//
//  Resource+GMSMarker.swift
//  Maap
//
//  Created by Borja on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import GoogleMaps

extension Resource {

    /// Generate a GMSMarker from a Resource
    var marker: GMSMarker? {

        let position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        let marker = GMSMarker(position: position)

        marker.title = self.name
        marker.snippet = "Company zone id: \(self.companyZoneId)"
        marker.icon = GMSMarker.markerImage(with: MaapColorHelper.color(for: self.companyZoneId))

        return marker
    }
}
