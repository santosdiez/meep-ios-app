//
//  DataSource.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation

/// The purpose of this enum is to store in one single place
/// references to the different implementations, so that
/// changing a implementation is as simple as changing the initializer
enum DataSource {

    static let resources: ResourcesDataSourceProtocol = NetworkResourcesDataSource()
}
