//
//  ResourcesDataSource.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import CoreLocation
import Foundation
import PromiseKit

/// Full online implementation of the ResourcesDataSourceProtocol
class NetworkResourcesDataSource: ResourcesDataSourceProtocol {

    func promiseForResources(for city: String,
                             lowerLeft: CLLocationCoordinate2D,
                             upperRight: CLLocationCoordinate2D) -> Promise<[Resource]> {

        return NetworkManager.promiseForRequest(.resources(city, lowerLeft, upperRight), with: [Resource].self)
    }
}
