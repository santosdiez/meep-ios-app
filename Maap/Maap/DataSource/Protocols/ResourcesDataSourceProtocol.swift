//
//  ResourcesDataSourceProtocol.swift
//  Maap
//
//  Created by Borja Santos-Díez on 12/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import CoreLocation
import Foundation
import PromiseKit

protocol ResourcesDataSourceProtocol: class {

    /// Retrieve the resources for a given city and area
    ///
    /// - Parameters:
    ///   - city: Name of the city
    ///   - lowerLeft: 2D coordinate defining the lower left limit
    ///   - upperRight: 2D coordinate defining the upper right limit
    /// - Returns: Promise to retrieve the existing resources for the given parameters
    func promiseForResources(for city: String,
                             lowerLeft: CLLocationCoordinate2D,
                             upperRight: CLLocationCoordinate2D) -> Promise<[Resource]>
}
