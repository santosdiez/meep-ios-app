//
//  InfoViewNavigator.swift
//  Maap
//
//  Created by Borja on 12/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import UIKit

protocol InfoViewNavigationProtocol: class {

    @discardableResult
    func navigateToMapView(from: InfoViewController) -> UIViewController?
}
