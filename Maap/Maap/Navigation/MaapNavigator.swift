//
//  MaapNavigator.swift
//  Maap
//
//  Created by Borja Santos-Díez on 12/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import UIKit

typealias MaapNavigatorProtocol = MapViewNavigatorProtocol & InfoViewNavigationProtocol

/// Implementation of the MaapNavigatorProtocol
class MaapNavigator: MaapNavigatorProtocol {

    @discardableResult
    func navigateToInfoView(from: MapViewController) -> UIViewController? {

        let infoViewController = InfoViewController(with: self)
        infoViewController.modalPresentationStyle = .formSheet
        infoViewController.modalTransitionStyle = .coverVertical

        from.present(infoViewController, animated: true, completion: nil)

        return infoViewController
    }

    @discardableResult
    func navigateToMapView(from: InfoViewController) -> UIViewController? {

        from.presentingViewController?.dismiss(animated: true, completion: nil)

        return from.presentingViewController
    }
}
