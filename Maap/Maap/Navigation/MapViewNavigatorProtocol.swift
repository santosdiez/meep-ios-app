//
//  MaapNavigatorProtocol.swift
//  Maap
//
//  Created by Borja Santos-Díez on 12/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import UIKit

protocol MapViewNavigatorProtocol: class {

    @discardableResult
    func navigateToInfoView(from: MapViewController) -> UIViewController?
}
