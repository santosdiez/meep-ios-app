//
//  UIColor+Utils.swift
//  Maap
//
//  Created by Borja on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import UIKit

/// Provide helper methods to deal with colors throughout the app
struct MaapColorHelper {

    private static var cachedColors: [Int: UIColor] = [:]

    /// Returns a random color associated to a specific companyZoneId.
    /// Colors will only be stored per session, meaning that after a reboot
    /// of the app, the generated colors will be different
    ///
    /// - Parameter companyZoneId: Identifier which will be used to retrieve the color
    /// - Returns: The associated color for the given companyZoneId
    static func color(for companyZoneId: Int) -> UIColor {

        if let color = self.cachedColors[companyZoneId] {

            return color
        }

        let color = UIColor(hue: CGFloat(arc4random_uniform(256))/255.0, saturation: 1, brightness: 1, alpha: 1)
        self.cachedColors[companyZoneId] = color

        return color
    }
}
