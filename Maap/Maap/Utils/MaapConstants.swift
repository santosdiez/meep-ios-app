//
//  MaapConstants.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation

enum MaapConstants {

    /// API Key for the Google Map services
    static let googleApiKey = "AIzaSyAvoAX7PwWnitlxQPhkW5QbBBp59zwEDcg"

    /// Default city to retrieve resources for
    static let sampleCity = "lisboa"

    /// Initial latitude for the map
    static let initialLat = 38.711046

    /// Initial longitude for the map
    static let initialLon = -9.160096
}
