//
//  NetworkManager.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import PromiseKit

struct GenericError: Error {}

struct NetworkManager {

    private static let jsonDecoder = JSONDecoder()

    static func promiseForRequest<T: Codable>(_ endpoint: APIEndpoint, with type: T.Type) -> Promise<T> {

        guard let request = self.createRequest(endpoint) else {

            fatalError("Error creating request")
        }

        return URLSession.shared.dataTask(.promise, with: request).then {

            return try Promise.value(self.jsonDecoder.decode(T.self, from: $0.data))
        }
    }

    static func promiseForRequest<T: Codable>(_ endpoint: APIEndpoint, with type: [T].Type) -> Promise<[T]> {

        guard var request = self.createRequest(endpoint) else {

            fatalError("Error creating request")
        }

        request.httpMethod = endpoint.httpMethod

        return URLSession.shared.dataTask(.promise, with: request).then {

            return try Promise.value(self.jsonDecoder.decode([T].self, from: $0.data))
        }
    }
}

// MARK: - Helper methods
private extension NetworkManager {

    static func createRequest(_ endpoint: APIEndpoint) -> URLRequest? {

        var components = URLComponents()
        components.scheme = APIEndpoint.scheme
        components.host = APIEndpoint.host
        components.path = endpoint.path
        components.queryItems = endpoint.queryItems?.compactMap {

            URLQueryItem(name: $0.key, value: $0.value)
        }

        guard let url = components.url else { return nil }

        var request = URLRequest(url: url)

        if let headers = endpoint.headers {

            headers.forEach {

                request.setValue($0.value, forHTTPHeaderField: $0.key)
            }
        }

        return request
    }
}
