//
//  APIEndpoint.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import CoreLocation
import Foundation

/// The idea behind this enum is to create a simple approach
/// for the existing endpoints
enum APIEndpoint {

    static let scheme = "https"
    static let host = "apidev.meep.me"
    private static let basePath = "/tripplan/api/v1"

    case resources(String, CLLocationCoordinate2D, CLLocationCoordinate2D)

    var path: String {

        let relativePath: String = {

            switch self {

            case .resources(let city, _, _):

                return "routers/\(city)/resources"
            }
        }()

        return "\(APIEndpoint.basePath)/\(relativePath)"
    }

    var httpMethod: String {

        switch self {

        case .resources:

            return "GET"
        }
    }

    var queryItems: [String: String?]? {

        switch self {

        case .resources(_, let lowerLeft, let upperRight):

            return [
                "lowerLeftLatLon": "\(lowerLeft.latitude),\(lowerLeft.longitude)",
                "upperRightLatLon": "\(upperRight.latitude),\(upperRight.longitude)"
            ]
        }
    }

    var headers: [String: String?]? {

        /// Common values for all cases
        /// Would be possible to do specific
        return ["Accept": "application/json"]
    }
}
