//
//  InfoViewController.swift
//  Maap
//
//  Created by Borja Santos-Díez on 12/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import GoogleMaps
import PureLayout
import UIKit

class InfoViewController: UIViewController {

    private unowned let navigator: MaapNavigator
    private let textView = UITextView.newAutoLayout()
    private let dismissButton = UIButton(type: .custom).configureForAutoLayout()

    init(with navigator: MaapNavigator) {

        self.navigator = navigator
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        self.configureView()
    }
}

// MARK: - View configuration
private extension InfoViewController {

    enum Constants {

        /// Margin for the close button
        static let buttonMargin: CGFloat = 15

        /// Size for the close button
        static let buttonSize: CGFloat = 30

        /// Inset for the text field
        static let textFieldInset: CGFloat = 30
    }

    func configureView() {

        self.addSubviews()
        self.addViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.view.addSubview(self.textView)
        self.view.addSubview(self.dismissButton)
    }

    func addViewConstraints() {

        self.textView.autoPinEdgesToSuperviewSafeArea()

        self.dismissButton.autoPinEdge(toSuperviewSafeArea: .top, withInset: Constants.buttonMargin)
        self.dismissButton.autoPinEdge(toSuperviewSafeArea: .right, withInset: Constants.buttonMargin)
        self.dismissButton.autoSetDimensions(to: CGSize(width: Constants.buttonSize, height: Constants.buttonSize))
    }

    func configureSubviews() {

        self.view.backgroundColor = .white

        self.dismissButton.setImage(Asset.iconClose.image, for: .normal)
        self.dismissButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)

        self.textView.isEditable = false
        self.textView.contentInset = UIEdgeInsets(top: Constants.textFieldInset/2,
                                                  left: Constants.textFieldInset,
                                                  bottom: Constants.textFieldInset/2,
                                                  right: Constants.textFieldInset)

        self.textView.text = GMSServices.openSourceLicenseInfo()
    }
}

// MARK: - Actions
private extension InfoViewController {

    @objc
    func navigateBack() {

        self.navigator.navigateToMapView(from: self)
    }
}
