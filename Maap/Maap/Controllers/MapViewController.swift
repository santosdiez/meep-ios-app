//
//  MainViewController.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation
import GoogleMaps
import PureLayout
import SVProgressHUD
import UIKit

class MapViewController: UIViewController {

    private let mapView = GMSMapView.newAutoLayout()
    private let infoButton = UIButton(type: .infoDark).configureForAutoLayout()
    private unowned let navigator: MaapNavigatorProtocol
    private unowned let dataSource: ResourcesDataSourceProtocol

    init(with navigator: MaapNavigatorProtocol,
         dataSource: ResourcesDataSourceProtocol) {

        self.navigator = navigator
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        self.configureView()

        self.mapView.delegate = self
        self.mapView.setMinZoom(Constants.minZoomLevel, maxZoom: Constants.maxZoomLevel)

        self.mapView.animate(to: GMSCameraPosition(latitude: MaapConstants.initialLat,
                                                   longitude: MaapConstants.initialLon,
                                                   zoom: Constants.zoomLevel))

        SVProgressHUD.show()
    }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {

    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {

        self.loadData()
    }
}

// MARK: - Data loading
private extension MapViewController {

    func loadData() {

        let visibleArea = self.mapView.projection.visibleRegion()

        self.dataSource.promiseForResources(for: MaapConstants.sampleCity,
                                            lowerLeft: visibleArea.nearLeft,
                                            upperRight: visibleArea.farRight).done { resources in

                                                self.displayResources(resources)
                                                SVProgressHUD.popActivity()
            }.catch { error in

                SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func displayResources(_ resources: [Resource]) {

        self.clearMap()

        resources.forEach {

            guard let marker = $0.marker else { return }
            marker.map = self.mapView
        }
    }

    func clearMap() {

        let selected = self.mapView.selectedMarker

        self.mapView.clear()

        if let selected = selected {

            selected.map = self.mapView
            self.mapView.selectedMarker = selected
        }
    }
}

// MARK: - View configuration
private extension MapViewController {

    enum Constants {

        /// Initial zoom level
        static let zoomLevel: Float = 16

        /// Min value for the zoom level
        static let minZoomLevel: Float = 15

        /// Max value for the zoom level
        static let maxZoomLevel: Float = 20

        /// Margin for the info button
        static let buttonMargin: CGFloat = 35
    }

    func configureView() {

        self.addSubviews()
        self.addViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.view.addSubview(self.mapView)
        self.view.addSubview(self.infoButton)
    }

    func addViewConstraints() {

        self.mapView.autoPinEdgesToSuperviewEdges()

        self.infoButton.autoPinEdge(toSuperviewSafeArea: .right, withInset: Constants.buttonMargin)
        self.infoButton.autoPinEdge(toSuperviewSafeArea: .bottom, withInset: Constants.buttonMargin/2)
    }

    func configureSubviews() {

        self.infoButton.addTarget(self, action: #selector(showInfo), for: .touchUpInside)
    }

    @objc
    func showInfo() {

        self.navigator.navigateToInfoView(from: self)
    }
}
