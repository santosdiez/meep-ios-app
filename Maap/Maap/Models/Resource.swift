//
//  Resource.swift
//  Maap
//
//  Created by Borja Santos-Díez on 11/06/2019.
//  Copyright © 2019 Borja Santos-Díez. All rights reserved.
//

import Foundation

struct Resource: Codable {

    //    "id": "402:11059006",
    //    "name": "Rossio",
    //    "x": -9.1424,
    //    "y": 38.71497,
    //    "companyZoneId": 402,
    //    "lat": 38.71497,
    //    "lon": -9.1424

    private enum CodingKeys: String, CodingKey {

        case id
        case name
        case latitude = "y"
        case longitude = "x"
        case companyZoneId
    }

    let id: String
    let name: String
    let latitude: Double
    let longitude: Double
    let companyZoneId: Int
}
